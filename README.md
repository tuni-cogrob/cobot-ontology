# Cobot ontology 

Ontology representing collaborative robots knowledge in the field of industrial robotics.

## Getting Started

The files can be used as is, just clone this repository in the directory of your choice

```  
git clone https://gitlab.com/tuni-cogrob/cobot-ontology.git
```

To edit the ontology files, the [Protege editor](https://protege.stanford.edu/) for example can be used.